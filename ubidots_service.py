import time

import com.pybsoft.raspberry.utils.dhtsensor as dht
import com.pybsoft.raspberry.iot.ubidots.ubidots as ud

if __name__ == '__main__':
	
	print("[INFO]: EXAMPLE OF USE OF DHT11 TO MEASURE TEMPERATURE AND SEND THE INFO TO UBIDOTS")
	
	#Setting Up the UBIDOTS connection configurations
	ubidots = ud.Ubidots()
	ubidots.setToken("BBFF-TLu6DRmQ1oN1vonhAQdKGywLoZ8e3M")
	ubidots.setDeviceLabel("master_rbp")

	#Setting up the DHT11
	dht11 = dht.DHT11()

	print("[INFO]: PREPARING TO READ. INFORMATION WILL BE SEND TO UBIDOTS. CHECK YOUR DASHBOARD")

	#Specifying the pin which Im going to read. I can change the pin whenever I wat
	pin = 4
	failure_counter = 0 
	max_failures = 10

	while(True):

		if (failure_counter > max_failures):
			print("[ERROR] Max number of allowed failures have been reached.")
			break



		temperature, humidity = dht11.read(pin)

		if((temperature is None) or (humidity is None)):
			print("[ERROR]: Error Reading temperature from Sensor connected to pin 4")
			failure_counter = failure_counter + 1

		else:

			variables = {}
			variables['temperature'] = temperature
			variables['humidity'] = humidity

			payload = ubidots.build_payload(variables)

			ret = ubidots.post_request(payload)

			if(ret != True):
				print("[ERROR]: Error connecting to ubidots: " + ubidots.getPostResult())
				failure_counter = failure_counter + 1

			time.sleep(5)


print("[INFO]: Finishing script")