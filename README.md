# raspberry

Projects for Raspberry Pi 3

This project includes some classes and modules to be used by any Raspberry Pi 3

This includes
 - IoT
 - Sensors and small actuators
 - Interaction with Arduino
 - Small Web apps
 - SDR

Classes and Libraries will be uploaded once they are tested

