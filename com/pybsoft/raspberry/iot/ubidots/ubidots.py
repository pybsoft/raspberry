'''
@author: Juan Reina

Based on Ubidots example to connect to UBIDOTS from raspberry Pi.
This is a class to be used to connect and upload any variable(s)
to a device in UBIDOTS

Token and device label must be 
'''

import time
import requests


class Ubidots:
    
    def __init__(self):
        self.URL = "http://things.ubidots.com"
        self.token = None
        self.device_label = None
        self.device_url = None
        self.post_attempts = 5
        self.exit_message = "No post has been done yet"

    def setToken(self,token_):
        if (not(token_ is None)):
            self.token = token_
            self.headers = {"X-Auth-Token": self.token, "Content-Type": "application/json"}
            return 0
        else:
            return 1

    def setDeviceLabel(self,label_):
        if (not(label_ is None)):
            self.device_label = label_
            self.device_url = "{}/api/v1.6/devices/{}".format(self.URL, self.device_label)
            return 0
        else:
            return 1

    def getToken(self):
        return self.token

    def getDeviceLabel(self):
        return self.device_label

    def getPostResult(self):
        return self.exit_message

    def build_payload(self,variables):
        '''
        This method input is a dictionary with the parameters to send to Ubidots
        By default, there is nothing to do since, the API will parse the dictionary as a JSON Object
        HEre only is cheked that the ubidots information has been already set         
        '''
        if ((self.token is None) or (self.device_label is None) or (variables is None) ):
            return {}

        variables_len = len(variables.keys())

        if(variables_len == 0):
            return {}

        return variables


    def post_request(self,payload):
        # Creates the headers for the HTTP requests
        self.exit_message =""

        if ((self.device_url is None) or (self.headers is None)):
            self.exit_message="Device label or Ubidots API Token must be set"
            return False
        elif (payload is None):
            elf.exit_message="Parameters to send to Ubidots have not been set"
 
        # Makes the HTTP requests
        status = 400
        attempts = 0
        while status >= 400 and attempts <= self.post_attempts:
            req = requests.post(url=self.device_url, headers=self.headers, json=payload)
            status = req.status_code
            attempts += 1
            time.sleep(1)

        # Processes results
        if status >= 400:
            self.exit_message = ("[ERROR] Could not send data after %s attempts, please check \
                your token credentials and internet connection" % self.post_attempts)
            return False

        self.exit_message = "[INFO] request made properly, your device is updated"
        return True

