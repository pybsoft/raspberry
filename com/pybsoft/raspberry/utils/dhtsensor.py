'''
@author Juan Rein
This class is used to read the data from an DHT11 sensor. Pin to read must be specified
Once the class is instantiated, the same object can read several pins

'''
import sys
import Adafruit_DHT


class DHT11:

	def __init__(self):
		self.humidity = 0.0
		self.temperature = 0.0
		self.read_sesult="No read call yet"
		self.last_pin=0

	def readResult(self):
		return self.read_sesult

	def getLasReadHumidity(self):
		return self.humidity

	def getLastReadTemperature(self):
		return self.temperature

	def read(self,pin_):
		'''
		This method will read the temeperature and humidity measured by the DHT sensor connected to the
		specified pin of Raspberry Jumper J8
		'''
		pin = 0

		try:
			pin = int(pin_)
		except ValueError:
			self.read_result="Given sensor pin number (Jumper J8) must be an integer"
			return None

		if(pin<=0):
			self.read_result="The given sensor pin number (Jumper J8) must be greater than 0"
			return None

		self.humidity, self.temperature = Adafruit_DHT.read_retry(11,pin)

		self.last_pin = pin

		self.read_result ="OK"

		return [self.temperature, self.humidity]


